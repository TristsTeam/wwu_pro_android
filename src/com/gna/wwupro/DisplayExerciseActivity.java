package com.gna.wwupro;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gna.wwupro.Infrastructure.BaseDisplayExerciseClass;
import com.gna.wwupro.Infrastructure.WWUCommon;

public class DisplayExerciseActivity extends BaseDisplayExerciseClass implements
		OnClickListener {

	int index = 0;
	int hours = 00;
	int minutes = 00;
	int seconds = 00;
	Thread myThread = null;
	Boolean running = true;
	boolean isTimeMode = false;
	boolean isPauseMode = false;
	int GENERALDIALOG = 101;
	int ComingBackFromCompleteDialog = 102;
	ImageButton pauseBtn;
	int pauseClickTime = 1;
	ImageView thirdImage;
	LinearLayout thirdTexTLayout;
	TextView thirdTextView;
	TextView timerText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.display_wrapup);
		keepScreenOn();
		intializeViews();
		intializeStates(); // Method in common base class
		if (getIntent().getExtras().getInt("value") == 1)
			ultimateExercise();
		else if (getIntent().getExtras().getInt("value") == 2)
			militaryMadnessExercise();
		else if (getIntent().getExtras().getInt("value") == 3)
			elevationExercise();
		else if (getIntent().getExtras().getInt("value") == 4)
			grooveAExercise();
		else if (getIntent().getExtras().getInt("value") == 5)
			grooveBExercise();
		else if (getIntent().getExtras().getInt("value") == 6)
			resistanceExercise();
		else if (getIntent().getExtras().getInt("value") == 7)
			fireAbsExercise();
		timerText = (TextView) findViewById(R.id.timerText);
		RelativeLayout finishLayout = (RelativeLayout) findViewById(R.id.finishLayout);
		TextView finishBtn = (TextView) findViewById(R.id.finishBtn);
		LinearLayout pauseBtnLayout = (LinearLayout) findViewById(R.id.pauseBarLayout);
		isTimeMode = getIntent().getExtras().getBoolean("isTimeMode");
		if (!isTimeMode) {
			pauseBtnLayout.setVisibility(View.GONE);
			finishLayout.setVisibility(View.GONE);
			timerText.setText("Finish");
			timerText.setOnClickListener(this);
			timerText.setTextColor(getResources().getColor(R.color.light_gray));
			index = getIntent().getExtras().getInt("selectedValue");

		} else {
			pauseBtn = (ImageButton) findViewById(R.id.pauseBarBtn);
			pauseBtn.setOnClickListener(this);
			pauseBtnLayout.setVisibility(View.VISIBLE);
			finishLayout.setVisibility(View.VISIBLE);
			finishBtn.setOnClickListener(this);
			Runnable runnable = new CountDownRunner();
			myThread = new Thread(runnable);
			myThread.start();
		}
		thirdImage = (ImageView) findViewById(R.id.thirdImage);
		thirdTexTLayout = (LinearLayout) findViewById(R.id.thirdTextLayout);
		thirdTextView = (TextView) findViewById(R.id.thirdTextView);

		rightArrowBtn.setOnClickListener(this);
		leftArrowBtn.setOnClickListener(this);
		emailButton.setOnClickListener(this);
		musicButton.setOnClickListener(this);
		infoBtn.setOnClickListener(this);
		cancelBtn.setOnClickListener(this);
		notesBarBtn.setOnClickListener(this);

		bindData();

	}

	private void bindData() {
		if (getIntent().getExtras().getInt("value") == 2) {
			if (index == 0) {
				thirdTexTLayout.setVisibility(View.VISIBLE);
				exerciseText.setVisibility(View.GONE);
				thirdImage.setVisibility(View.VISIBLE);
				firstImage.setBackgroundResource(R.drawable.mexercise01_01);
				secondImage.setBackgroundResource(R.drawable.mexercise01_02);
				thirdImage.setBackgroundResource(R.drawable.mexercise01_03);
				thirdTextView.setText(exerciseTextArray[index]);
			} else if (index == 3) {
				thirdTexTLayout.setVisibility(View.VISIBLE);
				exerciseText.setVisibility(View.GONE);
				thirdImage.setVisibility(View.VISIBLE);
				thirdImage.setVisibility(View.VISIBLE);
				firstImage.setBackgroundResource(R.drawable.mexercise04_01);
				secondImage.setBackgroundResource(R.drawable.mexercise04_02);
				thirdImage.setBackgroundResource(R.drawable.mexercise04_03);
				thirdTextView.setText(exerciseTextArray[index]);
			} else {
				thirdTexTLayout.setVisibility(View.GONE);
				thirdImage.setVisibility(View.GONE);
				exerciseText.setVisibility(View.VISIBLE);
				firstImage.setBackgroundResource(firstPictureArray[index]);
				secondImage.setBackgroundResource(secondPictureArray[index]);
				exerciseText.setText(exerciseTextArray[index]);
			}
		} else {
			thirdTexTLayout.setVisibility(View.GONE);
			thirdImage.setVisibility(View.GONE);
			firstImage.setBackgroundResource(firstPictureArray[index]);
			secondImage.setBackgroundResource(secondPictureArray[index]);
			exerciseText.setText(exerciseTextArray[index]);
		}
		exerciseName.setText(exerciseNameArray[index]);
		exerciseCount.setText("Exercise " + (index + 1));
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		if (view.getId() == R.id.rightArrowBtn) {
			if (index + 1 == exerciseNameArray.length)
				index = 0;
			else
				index += 1;
			bindData();
		} else if (view.getId() == R.id.leftArrowBtn) {
			if (index == 0)
				index = exerciseNameArray.length - 1;
			else
				index -= 1;
			bindData();
		} else if (view.getId() == R.id.emailBarBtn) {
			if (isTimeMode) {
				pauseThread();
				WWUCommon.getInstance(this).setHour(hours);
				WWUCommon.getInstance(this).setMinute(minutes);
				WWUCommon.getInstance(this).setSeconds(seconds);
			}
			email();
		} else if (view.getId() == R.id.musicBarBtn) {
			try {
				Intent intent = new Intent(android.content.Intent.ACTION_MAIN);
				intent.addCategory("android.intent.category.APP_MUSIC");
				if (isTimeMode) {
					pauseThread();
					WWUCommon.getInstance(this).setHour(hours);
					WWUCommon.getInstance(this).setMinute(minutes);
					WWUCommon.getInstance(this).setSeconds(seconds);
					startActivityForResult(intent, 1);
				} else
					startActivity(intent);
			} catch (ActivityNotFoundException e) {
				Intent intent = new Intent("android.intent.action.MUSIC_PLAYER");
				if (isTimeMode) {
					pauseThread();
					startActivityForResult(intent, 1);
				} else
					startActivity(intent);
			}
		} else if (view.getId() == R.id.infoBtn) {
			Intent intent = new Intent(this, NoofWrapsActvity.class);
			intent.putExtra("value", getIntent().getExtras().getInt("value"));
			intent.putExtra("comingFrom", "back");
			if (isTimeMode) {
				pauseThread();
				startActivityForResult(intent, 100);
			} else
				startActivity(intent);
		} else if (view.getId() == R.id.cancelBtn) {
			if (isTimeMode)
				pauseThread();
			Intent intent = new Intent(this, GenericDialogActivity.class);
			intent.putExtra("message", getString(R.string.Quit));
			intent.putExtra("isQuit", true);
			startActivityForResult(intent, GENERALDIALOG);
		} else if (view.getId() == R.id.finishBtn) {
			if (isTimeMode)
				pauseThread();
			Intent intent = new Intent(this, CompleteDialogActivity.class);
			intent.putExtra("CompleteValue", timerText.getText());
			intent.putExtra("isTimerMood", isTimeMode);
			startActivityForResult(intent, ComingBackFromCompleteDialog);
		} else if (view.getId() == R.id.notesBarBtn) {
			Intent intent = new Intent(this, NotesActivity.class);
			if (isTimeMode) {
				pauseThread();
				startActivityForResult(intent, 100);
			} else
				startActivity(intent);
		} else if (view.getId() == R.id.timerText) {
			Intent intent = new Intent(this, CompleteDialogActivity.class);
			intent.putExtra("CompleteValue", timerText.getText());
			intent.putExtra("isTimerMood", isTimeMode);
			startActivityForResult(intent, ComingBackFromCompleteDialog);
		} else if (view.getId() == R.id.pauseBarBtn) {
			if (pauseClickTime == 1) {
				pauseThread();
				isPauseMode = true;
				pauseBtn.setBackgroundResource(R.drawable.pausebar_down);
				pauseClickTime = 2;
				Intent intent = new Intent(this, GenericDialogActivity.class);
				intent.putExtra("message", getString(R.string.TimerPaused));
				intent.putExtra("isQuit", false);
				startActivity(intent);
			} else if (pauseClickTime == 2) {
				resumeThread();
				isPauseMode = false;
				pauseBtn.setBackgroundResource(R.drawable.pausebar_norm);
				pauseClickTime = 1;
			}
		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (isTimeMode)
			pauseThread();
		Intent intent = new Intent(this, GenericDialogActivity.class);
		intent.putExtra("message", getString(R.string.Quit));
		intent.putExtra("isQuit", true);
		startActivityForResult(intent, GENERALDIALOG);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == GENERALDIALOG) {
			if (resultCode == RESULT_OK) {
				Bundle extras = data.getExtras();
				if (extras != null) {
					boolean result = extras.getBoolean("dialogresult");
					if (result) {
						this.finish();
					}
				}
			}
		} else if (requestCode == ComingBackFromCompleteDialog) {
			if (resultCode == RESULT_OK) {
				this.finish();
			}
		} else if (requestCode == 1) {
			if (!isPauseMode) {
				hours = WWUCommon.getInstance(this).getHour();
				minutes = WWUCommon.getInstance(this).getMinute();
				seconds = WWUCommon.getInstance(this).getSeconds();

				WWUCommon.getInstance(this).setHour(0);
				WWUCommon.getInstance(this).setHour(0);
				WWUCommon.getInstance(this).setHour(0);
				resumeThread();
			}
		}
		if (isTimeMode) {
			if (!isPauseMode)
				resumeThread();
		}

	}

	public void doWork() {

		runOnUiThread(new Runnable() {
			public void run() {
				try {
					String secondString, minuteString, hourString;

					// ---------------------- for increment of
					// time----------------
					seconds++;
					if (seconds == 60) {
						minutes++;
						seconds = 00;
					}
					if (minutes == 60) {
						hours++;
						minutes = 00;
					}
					if (hours == 24) {
						hours = 0;
					}
					if (seconds < 10) {
						secondString = "0" + seconds;
					} else {
						secondString = "" + seconds;
					}
					if (minutes < 10) {
						minuteString = "0" + minutes;
					} else
						minuteString = "" + minutes;
					if (hours < 10)
						hourString = "0" + hours;
					else
						hourString = "" + hours;
					String curTime2 = hourString + ":" + minuteString + ":"
							+ secondString;
					timerText.setText(curTime2);
					timerText.setTextColor(getResources().getColor(
							R.color.blue_text));
				} catch (Exception e) {

				}
			}
		});

	}

	class CountDownRunner implements Runnable {
		// @Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				try {
					if (running) {
						doWork();
						Thread.sleep(1000);
					} else {

					}
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				} catch (Exception e) {
				}
			}
		}

	}

	public void pauseThread() {
		running = false;
	}

	public void resumeThread() {
		running = true;
	}
}

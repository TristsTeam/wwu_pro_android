package com.gna.wwupro;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

import com.gna.wwupro.Infrastructure.KeepScreenOnBaseClass;

public class CompleteDialogActivity extends KeepScreenOnBaseClass implements
		OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.complete_dialog);
		keepScreenOn();

		Button cancelBtn = (Button) findViewById(R.id.cancelBtn);
		cancelBtn.setOnClickListener(this);

		Button okBtn = (Button) findViewById(R.id.okBtn);
		okBtn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.cancelBtn)
			this.finish();
		else if (v.getId() == R.id.okBtn) {
			Intent congratulationIntent = new Intent(this,
					CongratulationActivity.class);
			congratulationIntent.putExtra("CompleteValue", getIntent()
					.getExtras().getString("CompleteValue"));
			congratulationIntent.putExtra("isTimerMood", getIntent()
					.getExtras().getBoolean("isTimerMood"));
			startActivity(congratulationIntent);

			Intent intent = new Intent();
			setResult(RESULT_OK, intent);
			finish();
		}
	}

}

package com.gna.wwupro;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gna.wwupro.Infrastructure.KeepScreenOnBaseClass;

public class NoofWrapsActvity extends KeepScreenOnBaseClass implements
		OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.noofwraps);
		keepScreenOn();

		ImageView titleView = (ImageView) findViewById(R.id.titleView);
		TextView exerciseValue = (TextView) findViewById(R.id.exercieValue);
		TextView repsValue = (TextView) findViewById(R.id.repsValue);
		TextView circutValue = (TextView) findViewById(R.id.circutsValue);
		TextView runsValue = (TextView) findViewById(R.id.runValue);
		TextView restValue = (TextView) findViewById(R.id.restValue);
		LinearLayout runsLayout = (LinearLayout) findViewById(R.id.runLayout);
		ImageButton leftArrowButton = (ImageButton) findViewById(R.id.leftArrowClick);
		ImageButton rightArrowButton = (ImageButton) findViewById(R.id.rightArrowClick);
		leftArrowButton.setOnClickListener(this);
		rightArrowButton.setOnClickListener(this);
		if (getIntent().getExtras().getString("comingFrom").equals("back"))
			rightArrowButton.setVisibility(View.INVISIBLE);
		else
			rightArrowButton.setVisibility(View.VISIBLE);

		switch (getIntent().getExtras().getInt("value")) {
		case 1:
			titleView.setBackgroundResource(R.drawable.title_ultimate_wrapup);
			exerciseValue.setText("5");
			repsValue.setText("10");
			circutValue.setText("4");
			restValue.setText("60");
			runsLayout.setVisibility(View.GONE);
			break;
		case 2:
			titleView.setBackgroundResource(R.drawable.title_military_madness);
			exerciseValue.setText("5");
			repsValue.setText("10");
			circutValue.setText("4");
			restValue.setText("60");
			runsLayout.setVisibility(View.GONE);
			break;
		case 3:
			titleView.setBackgroundResource(R.drawable.title_elevation);
			exerciseValue.setText("6");
			repsValue.setText("10");
			circutValue.setText("3");
			restValue.setText("60");
			runsLayout.setVisibility(View.GONE);
			break;
		case 4:
			titleView.setBackgroundResource(R.drawable.title_groovea);
			exerciseValue.setText("9");
			repsValue.setText("15");
			circutValue.setText("2-3");
			restValue.setText("60");
			runsValue.setText("10");
			runsLayout.setVisibility(View.VISIBLE);
			break;
		case 5:
			titleView.setBackgroundResource(R.drawable.title_grooveb);
			exerciseValue.setText("7");
			repsValue.setText("15");
			circutValue.setText("2-3");
			restValue.setText("60");
			runsValue.setText("10");
			runsLayout.setVisibility(View.VISIBLE);
			break;
		case 6:
			titleView.setBackgroundResource(R.drawable.title_resistance);
			exerciseValue.setText("5");
			repsValue.setText("12");
			circutValue.setText("4");
			restValue.setText("60");
			runsLayout.setVisibility(View.GONE);
			break;
		case 7:
			titleView.setBackgroundResource(R.drawable.title_fireabc);
			exerciseValue.setText("4");
			repsValue.setText("15");
			circutValue.setText("3-4");
			restValue.setText("30");
			runsLayout.setVisibility(View.GONE);
			break;
		}

	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		if (view.getId() == R.id.leftArrowClick) {
			if (getIntent().getExtras().getString("comingFrom").equals("back"))
				this.finish();
			else {
				Intent intent = new Intent(this, ModeSelectionActivity.class);
				intent.putExtra("value", getIntent().getExtras()
						.getInt("value"));
				startActivity(intent);
				this.finish();
			}
		} else if (view.getId() == R.id.rightArrowClick) {
			if (getIntent().getExtras().getBoolean("isTimedMode")) {
				Intent intent = new Intent(this, TimeOnWrapUpActivity.class);
				intent.putExtra("value", getIntent().getExtras()
						.getInt("value"));
				startActivity(intent);
				this.finish();
			} else {
				Intent intent = new Intent(this, TimeOffWrapUpActivity.class);
				intent.putExtra("value", getIntent().getExtras()
						.getInt("value"));
				startActivity(intent);
				this.finish();
			}
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (getIntent().getExtras().getString("comingFrom").equals("back"))
			this.finish();
		else {
			Intent intent = new Intent(this, ModeSelectionActivity.class);
			intent.putExtra("value", getIntent().getExtras().getInt("value"));
			startActivity(intent);
			this.finish();
		}
	}
}

package com.gna.wwupro;

import com.gna.wwupro.Infrastructure.KeepScreenOnBaseClass;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.URLSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

public class InfoActivity extends KeepScreenOnBaseClass implements
		OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.info);
		intializeViews();
		keepScreenOn();
	}

	private void intializeViews() {

		TextView joinUs = (TextView) findViewById(R.id.joinUsTxt);
		TextView aboutWWU = (TextView) findViewById(R.id.aboutWWUTxt);
		TextView whatWWU = (TextView) findViewById(R.id.whatWWUTxt);
		TextView instruction = (TextView) findViewById(R.id.instructionTxt);
		TextView share = (TextView) findViewById(R.id.shareTxt);
		TextView support = (TextView) findViewById(R.id.supportTxt);
		ImageButton leftArrowClick = (ImageButton) findViewById(R.id.leftArrowClick);

		joinUs.setOnClickListener(this);
		aboutWWU.setOnClickListener(this);
		whatWWU.setOnClickListener(this);
		instruction.setOnClickListener(this);
		share.setOnClickListener(this);
		support.setOnClickListener(this);
		leftArrowClick.setOnClickListener(this);

	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		if (view.getId() == R.id.leftArrowClick) {
			this.finish();
		} else if (view.getId() == R.id.supportTxt) {
			email();
		} else if (view.getId() == R.id.shareTxt) {
			Intent intent = new Intent(this, ShareActivity.class);
			startActivity(intent);
		} else if (view.getId() == R.id.joinUsTxt) {
			Intent myWebLink = new Intent(android.content.Intent.ACTION_VIEW);
			myWebLink
					.setData(Uri
							.parse("http://www.ultimatefitnessapp.com/join-us-at-ultimate-fitness-app-vip-insider/"));
			startActivity(myWebLink);
		} else if (view.getId() == R.id.aboutWWUTxt) {
			Intent intent = new Intent(this, WWUActivity.class);
			intent.putExtra("value", 1);
			startActivity(intent);
		} else if (view.getId() == R.id.whatWWUTxt) {
			Intent intent = new Intent(this, WWUActivity.class);
			intent.putExtra("value", 2);
			startActivity(intent);
		} else if (view.getId() == R.id.instructionTxt) {
			Intent intent = new Intent(this, WWUActivity.class);
			intent.putExtra("value", 3);
			startActivity(intent);
		}
	}

	private void email() {

		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL,
				new String[] { "Support@UltimateFitnessApp.com" });
		i.putExtra(Intent.EXTRA_SUBJECT, "WWU Support Ticket");

		this.startActivity(Intent.createChooser(i, "Select application"));

	}

}

package com.gna.wwupro.Infrastructure;

import com.gna.wwupro.R;

import android.graphics.drawable.StateListDrawable;
import android.widget.ImageButton;
import android.widget.TextView;



public class BaseExerciseClass extends CommonBaseClass {

	protected ImageButton leftArrowBtn;
	protected ImageButton infoBtn;
	protected TextView displayExercise1;
	protected TextView displayExercise2;
	protected TextView displayExercise3;
	protected TextView displayExercise4;
	protected TextView displayExercise5;
	protected TextView displayExercise6;
	protected TextView displayExercise7;
	protected TextView displayExercise8;
	protected TextView displayExercise9;
	protected ImageButton rightArrowBtn1;
	protected ImageButton rightArrowBtn2;
	protected ImageButton rightArrowBtn3;
	protected ImageButton rightArrowBtn4;
	protected ImageButton rightArrowBtn5;
	protected ImageButton rightArrowBtn6;
	protected ImageButton rightArrowBtn7;
	protected ImageButton rightArrowBtn8;
	protected ImageButton rightArrowBtn9;

	public void intializeView() {
		leftArrowBtn = (ImageButton) findViewById(R.id.leftArrowBtn);
		infoBtn = (ImageButton) findViewById(R.id.infoBtn);

		StateListDrawable infoBtnStates = new StateListDrawable();
		infoBtnStates.addState(new int[] { android.R.attr.state_pressed }, this
				.getResources().getDrawable(R.drawable.infobar_down));
		infoBtnStates.addState(new int[] {},
				this.getResources().getDrawable(R.drawable.infobar_norm));
		infoBtn.setBackgroundDrawable(infoBtnStates);

		displayExercise1 = (TextView) findViewById(R.id.displayExercise1);
		displayExercise2 = (TextView) findViewById(R.id.displayExercise2);
		displayExercise3 = (TextView) findViewById(R.id.displayExercise3);
		displayExercise4 = (TextView) findViewById(R.id.displayExercise4);
		rightArrowBtn1 = (ImageButton) findViewById(R.id.rightArrowBtn1);
		rightArrowBtn2 = (ImageButton) findViewById(R.id.rightArrowBtn2);
		rightArrowBtn3 = (ImageButton) findViewById(R.id.rightArrowBtn3);
		rightArrowBtn4 = (ImageButton) findViewById(R.id.rightArrowBtn4);
		if (getIntent().getExtras().getInt("value") == 1
				|| getIntent().getExtras().getInt("value") == 6) {
			displayExercise5 = (TextView) findViewById(R.id.displayExercise5);
			rightArrowBtn5 = (ImageButton) findViewById(R.id.rightArrowBtn5);
		} else if (getIntent().getExtras().getInt("value") == 3
				|| getIntent().getExtras().getInt("value") == 2) {
			displayExercise5 = (TextView) findViewById(R.id.displayExercise5);
			rightArrowBtn5 = (ImageButton) findViewById(R.id.rightArrowBtn5);
			displayExercise6 = (TextView) findViewById(R.id.displayExercise6);
			rightArrowBtn6 = (ImageButton) findViewById(R.id.rightArrowBtn6);
		} else if (getIntent().getExtras().getInt("value") == 4) {
			displayExercise5 = (TextView) findViewById(R.id.displayExercise5);
			rightArrowBtn5 = (ImageButton) findViewById(R.id.rightArrowBtn5);
			displayExercise6 = (TextView) findViewById(R.id.displayExercise6);
			rightArrowBtn6 = (ImageButton) findViewById(R.id.rightArrowBtn6);
			displayExercise7 = (TextView) findViewById(R.id.displayExercise7);
			displayExercise8 = (TextView) findViewById(R.id.displayExercise8);
			displayExercise9 = (TextView) findViewById(R.id.displayExercise9);
			rightArrowBtn7 = (ImageButton) findViewById(R.id.rightArrowBtn7);
			rightArrowBtn8 = (ImageButton) findViewById(R.id.rightArrowBtn8);
			rightArrowBtn9 = (ImageButton) findViewById(R.id.rightArrowBtn9);
		} else if (getIntent().getExtras().getInt("value") == 5) {
			displayExercise5 = (TextView) findViewById(R.id.displayExercise5);
			rightArrowBtn5 = (ImageButton) findViewById(R.id.rightArrowBtn5);
			displayExercise6 = (TextView) findViewById(R.id.displayExercise6);
			rightArrowBtn6 = (ImageButton) findViewById(R.id.rightArrowBtn6);
			displayExercise7 = (TextView) findViewById(R.id.displayExercise7);
			rightArrowBtn7 = (ImageButton) findViewById(R.id.rightArrowBtn7);
		}
	}

}
